import csv
import requests

from common import HrefContainer, redirect_301, read_credentials, write_results


def read_to_hrefs(csv_path):
    hrefs = []
    with open(csv_path, mode='r',  encoding="utf8") as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        next(reader)  # skipping header
        for row in reader:
            src = row[1]
            dst = row[2]
            selector = row[12]
            hrefs.append(HrefContainer(src=src, dst=dst, selector=selector))
    return hrefs


def main():
    csv_path = 'crawl.csv'
    out_path = 'crawl_redirected.csv'
    user, pw = read_credentials('password.csv')
    autodev = True
    hrefs = read_to_hrefs(csv_path)
    i = 0
    cache = {}
    for href in hrefs:
        new_url = href.dst
        if "go-to-broker" in href.dst:
            print(f'Processing {i}/{len(hrefs)}')
            if href.dst in cache:
                new_url = cache[href.dst]
                print('cached')
            else:
                try:
                    new_url = redirect_301(href.dst, autodev, user, pw)
                except requests.exceptions.ReadTimeout:
                    print(f'TimeoutException for {new_url}')
                    pass  # todo maybe here a selenium
                except Exception:
                    pass
                cache.update({href.dst: new_url})
            print(f'old:{href.dst}')
            print(f'new: {new_url}')
            href.dst = new_url
        i += 1

    write_results(out_path, hrefs)
    return


if __name__ == '__main__':
    main()