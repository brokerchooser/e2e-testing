import csv
from dataclasses import dataclass

import requests
from dataclass_csv import DataclassWriter


@dataclass(eq=True, order=True)
class HrefContainer:
    src: str
    dst: str
    selector: str

    # Needs to be implemented in order to have hash
    def __hash__(self):
        return hash(self.src + self.dst + self.selector)


def read_credentials(path):
    """
    This function reads and the username and password from a given .csv
    Format should be:
        user,[username]
        password,[password]
    If the given csv is not found or the format does not match, error message is displayed
    :param path: path of the csv containing credentiols
    :return: list containing username and passord
    """
    credentials = []
    with open(path, mode='r') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        for row in reader:
            credentials.append(row[1])
    return credentials


def write_results(csv_path, hrefs):
    """
    The function writes the result of the scraping in a csv file
    :return:
    """
    with open(csv_path, "w", newline='') as f:
        w = DataclassWriter(f, list(hrefs), HrefContainer)
        w.map("src").to("Source")
        w.map("dst").to("Destination")
        w.map("selector").to("Link Path")
        w.write()


def redirect_301(url, autodev, user, pw):
    """
    This function searches the first URL that corresponds to HTTP return code 302. If there is no 302 return code,
    the last path of the return chain is returned
    :param url:
    :param autodev:
    :param user:
    :param pw:
    :return:
    """
    if autodev:
        response = requests.get(url, timeout=30, auth=requests.auth.HTTPBasicAuth(user, pw))
    else:
        response = requests.get(url, timeout=30)
    url_302 = response.url
    if response.history:
        # print("url is redirected")
        for resp in response.history:
            # print(resp.status_code, resp.url)
            if resp.status_code == 301:
                url_302 = resp.url
                break
    else:
        print('not redirceted')
    return url_302