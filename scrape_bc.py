import time

import requests
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.common import exceptions
from selenium.webdriver.support.select import Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains

from common import HrefContainer, write_results, read_credentials, redirect_301


class LevelScraper:
    def __init__(self):
        self.options = {}
        self.indeces = []


class ScrapedLinks:
    FMB_TOP5_SELECTOR = "[@class=top-5-list-visit-broker-btn]"
    FMB_QUICK_PICKS_SELECTOR = "[@class=quick-picks-stage-visit-broker-btn]"
    COMPARE_BROKER_SELECTOR = "[@class=scrolling-list]"

    def __init__(self, driver, base_path, level_scraper, countries, autodev, user, pw):
        self.driver = driver
        self.base_path = base_path
        self.level_scraper = level_scraper
        self.countries = countries
        self.autodev = autodev
        self.user = user
        self.pw = pw
        self.curr_level = base_path
        self.prev_level = base_path
        self.hrefs = set()
        self.redirect_dict = {}

    def __del__(self):
        self.driver.quit()

    def __repr__(self):
        text = f'Scraped links, contains {len(self.hrefs)} links:'
        for curr_href in self.hrefs:
            text += '\n\t' + str(curr_href)
        return text

    def get_element(self, by, selector):
        try:
            element = WebDriverWait(self.driver, 10).until(lambda driver: driver.find_element(by, selector))
            return element
        except (exceptions.NoSuchElementException, exceptions.TimeoutException) as e:
            print('get_element failed')
        return

    def get_elements(self, by, selector):
        try:
            element = WebDriverWait(self.driver, 10).until(lambda driver: driver.find_elements(by, selector))
            return element
        except (exceptions.NoSuchElementException, exceptions.TimeoutException) as e:
            print('get_elements failed')
        return

    def list_ahrefs(self, root_xpath, selector, only_externals=True):
        """
        This function processes the current HTML page, finds all a hrefs and appends them to the class
        :param root_xpath: xptah of root of the top level app page
        :param selector: selector of the link, this selector will be exported
        :param only_externals: if True only the external links will be saved
        :return:
        """
        react_root = self.get_element(By.XPATH, root_xpath)
        hrefs = react_root.find_elements(By.XPATH, ".//a[@href]")
        redirect_url = "go-to-broker"
        href_idx = 0
        while href_idx < len(hrefs):
            hrefs = react_root.find_elements(By.XPATH, ".//a[@href]")  # necessary due to staleElementException
            link_path = hrefs[href_idx].get_attribute("href")
            if not only_externals or 'brokerchooser.com' not in link_path or redirect_url in link_path:
                # link_class = href.get_attribute("class") we will see
                self.hrefs.add(HrefContainer(src=self.curr_level, dst=link_path,
                                             selector=selector))
            href_idx += 1
        return

    def map_redirect(self, dev_server_name="autodev1"):
        """
        The function find the redirect links (go-to-broker) in HrefContainers and replaced them with the target,
        assuming the redirect link is the following format: https://brokerchooser.com/go-to-broker/BROKER_NAME
        Note: this function also takes a lot of time. In order to speed it up mapping and hashing has been implemented,
        but still quite slow
        :return:
        """
        redirect_url = ["go-to-broker"]  # , "affi"]
        for hrefContainer in self.hrefs:

            # update src as well
            #src_url_cleaned = hrefContainer.src.replace(dev_server_name + '.', '')
            #hrefContainer.src = src_url_cleaned

            to_redirect = False
            for redir in redirect_url:
                if redir in hrefContainer.dst:
                    to_redirect = True
            if to_redirect:
                broker_name = hrefContainer.dst.rsplit('/', 2)[1]
                #if broker_name in self.redirect_dict:
                #    hrefContainer.dst = self.redirect_dict[broker_name]
                #else:
                new_url = hrefContainer.dst
                try:
                    new_url = redirect_301(hrefContainer.dst, self.autodev, self.user, self.pw)
                except requests.exceptions.ReadTimeout:
                    self.driver.get(hrefContainer.dst)
                    new_url = self.driver.current_url
                except Exception:
                    pass
                hrefContainer.dst = new_url
                self.redirect_dict.update({broker_name: new_url})

                    #  try:
                    #    if autodev==True:
                    #        dst_url_cleaned=hrefContainer.dst.replace(dev_server_name+'.', '')

                    #    else:
                    #        dst_url_cleaned=hrefContainer.dst

                    #    res = requests.get(dst_url_cleaned, timeout=5)
                    #  except requests.exceptions.ReadTimeout:
                    #    pass
                    #  else:
                    #    res=self.driver.get(hrefContainer.dst)
                    #    new_url=self.driver.current_url
                    #  new_url = res.url
                    #  hrefContainer.dst = new_url
                    #    self.redirect_dict.update({broker_name: new_url})

    def down_one_level(self, xpath):
        time.sleep(0.5)
        btn = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, xpath)))
        btn.click()

        self.prev_level = self.curr_level
        self.curr_level = self.driver.current_url

    def scrape_level(self, level, final=False):
        """
        This is the main scraper function.
        Should be called recursively with increasing level parameter.
        :param level: current level of scraping
        :param final: if true this is the last level
        :return: none, ScrapedLinks extended with links
        """
        root_xpath = '//*[@id="react-app-root"]'
        if final:  # the final level is the quick picks of find my broker
            self.list_ahrefs(root_xpath=root_xpath, selector=self.FMB_QUICK_PICKS_SELECTOR)
            self.driver.get(self.base_path)
            return
        else:
            btn_xpath = ".//a[contains(text(), 'Next')]"
            form_items = self.get_elements(By.XPATH, ".//form/child::label")  # searching for all of the option
            # If not already saved in the options dict, we add them
            if len(self.level_scraper.indeces) <= level:
                form_items_dict = {level: form_items}
                self.level_scraper.options.update(form_items_dict)
                self.level_scraper.indeces.append(0)
            form_items[self.level_scraper.indeces[level]].click()
            final = False
            # the final level is 4, the finish of quick results
            if level == 3:
                final = True
                # We go to the next option in the previous level if the next if final
                self.level_scraper.indeces[level] += 1
            # If the next level is already at the end
            if (not final and len(self.level_scraper.indeces) > level + 1
                    and self.level_scraper.indeces[level + 1] == len(self.level_scraper.options.get(level + 1))):
                self.level_scraper.indeces[level + 1] = 0  # reset the next level
                self.level_scraper.indeces[level] += 1  # go to the next option of the current level
            self.list_ahrefs(root_xpath=root_xpath, selector=self.FMB_TOP5_SELECTOR)
            self.down_one_level(btn_xpath)
            self.scrape_level(level + 1, final)

    def scrape_level_0(self):
        """
        Scraping of the first page where you can select country
        :return: none, ScrapedLinks extended with links
        """
        idx = 0
        level = 0
        options_dict = {0: self.countries}
        btn_xpath = ".//a[contains(text(), 'Find the best broker for me')]"
        self.level_scraper.options = options_dict
        self.level_scraper.indeces.append(0)

        self.list_ahrefs(root_xpath='//*[@id="react-app-root"]', selector=self.FMB_TOP5_SELECTOR)
        while idx < len(self.countries):  # main loop: looping through the list of countries
            self.level_scraper.indeces[0] = idx
            select = Select(self.get_element(By.XPATH, '//select'))
            select.select_by_visible_text(self.countries[idx])
            # Only go to the next element in the countries list, if the first level is at the end
            if (len(self.level_scraper.indeces) > level + 1
                    and self.level_scraper.indeces[level + 1] == len(self.level_scraper.options.get(level + 1))):
                idx += 1
                self.level_scraper.indeces[level + 1] = 0  # resetting next level
            self.list_ahrefs(root_xpath='//*[@id="react-app-root"]', selector=self.FMB_TOP5_SELECTOR)
            self.down_one_level(btn_xpath)
            self.scrape_level(1)
        return

    def scrape_find_my_broker(self):
        """
        This is the entry point of the find my broker scraping.
        :return:
        """
        self.driver.get(self.base_path)
        """
        if on the autodev server, the rest part of the page does not load with https://{user}:{pw}@{fmb_base_path}
        but it saves the credentials for that given session.
        A workaround is that to set the url to https://{fmb_base_path}
        """
        if self.autodev:
            base_url = self.base_path.rsplit('@', 2)[1]
            self.base_path = f'https://{base_url}'
            self.driver.get(self.base_path)
        self.scrape_level_0()

    def iterate_compare_brokers(self):
        """
        This function iterates through a page of compared brokers.
        In the navigation bar we step not for each element but we step a whole screen wide (in 1920px it is 5 brokers)
        :return:
        """
        # First we determine how much can we step. Basically we read the total number of brokers and subtract
        # the largest data-index of MuiSlider-mark
        text_to_match = 'Currently you see'
        parent_elem = self.get_element(By.XPATH, f'//span[contains(text(),"{text_to_match}")]')
        num_of_brokers = 0
        for w in parent_elem.text.split():
            if w.isdigit():
                num_of_brokers = int(w)

        max_data_index = 0
        muiSliders = self.get_elements(By.XPATH, '//span[contains(@class,"MuiSlider-mark")]')
        for muiSlider in muiSliders:
            max_data_index = max(int(muiSlider.get_attribute('data-index')), max_data_index)

        broker_in_page = num_of_brokers - max_data_index
        for curr_data_index in range(num_of_brokers - broker_in_page + 2):
            if curr_data_index % broker_in_page == 0 or curr_data_index == num_of_brokers - broker_in_page + 1:
                time.sleep(1)
                self.list_ahrefs(root_xpath='//*[@id="compare-broker-app"]', selector=self.COMPARE_BROKER_SELECTOR)
            # after the first page there are 2 identical arrow, so we choose the later
            next_btn = self.get_elements(By.XPATH, '//div[contains(@class,"h-12 w-12")]')[-1]
            ac = ActionChains(self.driver)
            ac.move_to_element(next_btn).click().perform()

    def scrape_compare_countries(self):
        """
        This function find the correct select for country scraping and iterates over them
        :return:
        """
        # there are multiple selects in the react app which are indistinguishable for the scraper
        # Strategy: find the one that has matching selectable items as the country list
        selects = self.get_elements(By.XPATH, '//select')
        for curr_select in selects:
            select_obj = Select(curr_select)
            try:
                select_obj.select_by_visible_text(self.countries[0])
            except exceptions.NoSuchElementException:
                pass
            else:
                select = select_obj
                break  # no exception means we found the right one

        for country in self.countries:
            time.sleep(2)
            select.select_by_visible_text(country)
            self.iterate_compare_brokers()

    def scrape_compare_brokers(self):
        """
        This is the entry point of the compare brokers scraping.
        :return:
        """
        self.driver.get(self.base_path)
        if self.autodev:  # do not export credentials
            base_url = self.base_path.rsplit('@', 2)[1]
            self.base_path = f'https://{base_url}'
            self.curr_level = self.base_path
            self.prev_level = self.base_path
            self.driver.get(self.base_path)
        self.scrape_compare_countries()


def main():
    autodev = True
    # # we should start at stageIndex=0 to select test with different countries as well
    fmb_path = 'https://brokerchooser.com/find-my-broker?stageIndex=0'
    compare_path = 'https://brokerchooser.com/compare'
    # fmb_path = 'https://autodev1.brokerchooser.com/find-my-broker?stageIndex=0'
    # compare_path = 'https://autodev1.brokerchooser.com/compare'
    csv_path = 'output.csv'
    user, pw = read_credentials('password.csv')
    if autodev:
        fmb_base_path = 'autodev1.brokerchooser.com/find-my-broker?stageIndex=0'
        compare_base_path = 'autodev1.brokerchooser.com/compare'
        fmb_path = f'https://{user}:{pw}@{fmb_base_path}'
        compare_path = f'https://{user}:{pw}@{compare_base_path}'

    countries = ['Hungary']  # , 'Australia' , 'Canada', 'Germany', 'India', 'United Kingdom', 'United States']

    driver = webdriver.Firefox()
    level_scraper = LevelScraper()
    scrape_obj = ScrapedLinks(driver, fmb_path, level_scraper, countries, autodev, user, pw)

    print("Start scraping find my broker")
    scrape_obj.scrape_find_my_broker()
    print("End scraping find my broker")

    scrape_obj.base_path = scrape_obj.curr_level = scrape_obj.prev_level = compare_path
    print("Start scraping compare brokers")
    scrape_obj.scrape_compare_brokers()
    print("End scraping compare brokers")
    scrape_obj.map_redirect(scrape_obj.hrefs)
    print("Writing the results...")
    write_results(csv_path, scrape_obj.hrefs)

    print(scrape_obj)


if __name__ == '__main__':
    main()
